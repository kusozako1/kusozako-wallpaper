# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ConfigKeys


class DeltaPixbuf(DeltaEntity):

    def paint(self, cairo_context):
        current_image = self._enquiry("delta > current image")
        image_size = self._enquiry("delta > config", ConfigKeys.IMAGE_SIZE)
        pixbuf = current_image.scale_simple(
            current_image.get_width()*image_size,
            current_image.get_height()*image_size,
            GdkPixbuf.InterpType.BILINEAR
            )
        x_align = self._enquiry("delta > config", ConfigKeys.IMAGE_ALIGN_X)
        y_align = self._enquiry("delta > config", ConfigKeys.IMAGE_ALIGN_Y)
        x = (self._monitor_geometry.width-pixbuf.get_width())*x_align
        y = (self._monitor_geometry.height-pixbuf.get_height())*y_align
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, x, y)
        cairo_context.paint()

    def __init__(self, parent):
        self._parent = parent
        for monitor in Gdk.Display.get_default().get_monitors():
            self._monitor_geometry = monitor.get_geometry()
            break
