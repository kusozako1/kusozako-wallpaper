# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gdk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ConfigKeys


class DeltaSurface(DeltaEntity):

    def save(self):
        pixbuf = Gdk.pixbuf_get_from_surface(
            self._surface,
            0,
            0,
            self._monitor_geometry.width,
            self._monitor_geometry.height
            )
        id_ = self._enquiry("delta > data", "application-id")
        names = [GLib.get_user_config_dir(), id_, "wallpaper.png"]
        path = GLib.build_filenamev(names)
        success = pixbuf.savev(path, "png", [], [])
        return path if success else None

    def get_cairo_context(self):
        cairo_context = cairo.Context(self._surface)
        color = self._enquiry("delta > config", ConfigKeys.BACKGROUND_COLOR)
        cairo_context.set_source_rgba(*color)
        cairo_context.rectangle(
            0,
            0,
            self._surface.get_width(),
            self._surface.get_height()
            )
        cairo_context.fill()
        return cairo_context

    def __init__(self, parent):
        self._parent = parent
        for monitor in Gdk.Display.get_default().get_monitors():
            self._monitor_geometry = monitor.get_geometry()
            break
        self._surface = cairo.ImageSurface(
            cairo.Format.ARGB32,
            self._monitor_geometry.width,
            self._monitor_geometry.height
            )
