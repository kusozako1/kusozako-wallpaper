# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

FIT_TO_WIDTH = "fit-to-width"
FIT_TO_HEIGHT = "fit-to-height"
USER_DEFINED = "user-defined"
ORIGINAL = "original"
