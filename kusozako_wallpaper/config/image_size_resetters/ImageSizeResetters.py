# (c) copyright 2023-2025, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity
from .Original import DeltaOriginal
from .FitToHeight import DeltaFitToHeight
from .FitToWidth import DeltaFitToWidth


class DeltaImageSizeResetters(DeltaEntity):

    def _delta_info_monitor_geometry(self):
        return self._monitor_geometry

    def _initialize_monitor_geometry(self):
        for monitor in Gdk.Display.get_default().get_monitors():
            self._monitor_geometry = monitor.get_geometry()
            break

    def __init__(self, parent):
        self._parent = parent
        self._initialize_monitor_geometry()
        DeltaOriginal(self)
        DeltaFitToHeight(self)
        DeltaFitToWidth(self)
