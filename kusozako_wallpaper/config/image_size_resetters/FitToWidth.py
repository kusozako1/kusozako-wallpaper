# (c) copyright 2025, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_wallpaper.const import ImageSizeTypes
from .ImageSizeResetter import AlfaImageSizeResetter


class DeltaFitToWidth(AlfaImageSizeResetter):

    __config_value__ = ImageSizeTypes.FIT_TO_WIDTH

    def _get_new_ratio(self, pixbuf):
        monitor_geometry = self._enquiry("delta > monitor geometry")
        return monitor_geometry.width / pixbuf.get_width()
