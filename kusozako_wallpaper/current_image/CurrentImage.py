# (c) copyright 2023-2024, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity
from .InitialSetup import DeltaInitialSetup
from .observers.FileOpened import DeltaFileOpened


class DeltaCurrentImage(DeltaEntity):

    def get_current_image(self):
        return self._current_image

    def _delta_call_setup_current_image(self, gfile):
        path = gfile.get_path()
        self._current_image = GdkPixbuf.Pixbuf.new_from_file(path)

    def __init__(self, parent):
        self._parent = parent
        self._current_image = None
        DeltaInitialSetup(self)
        DeltaFileOpened(self)
