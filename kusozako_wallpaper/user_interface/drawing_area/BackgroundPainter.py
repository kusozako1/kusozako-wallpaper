# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ConfigKeys


class DeltaBackgroundPainter(DeltaEntity):

    def paint(self, cairo_context, width, height):
        clip_rectangle = self._enquiry("delta > clip rectangle")
        background_color = self._enquiry(
            "delta > config",
            ConfigKeys.BACKGROUND_COLOR
            )
        cairo_context.set_source_rgba(*background_color)
        cairo_context.rectangle(
            clip_rectangle.x,
            clip_rectangle.y,
            clip_rectangle.width,
            clip_rectangle.height,
            )
        cairo_context.fill()

    def __init__(self, parent):
        self._parent = parent
