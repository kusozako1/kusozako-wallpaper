# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from kusozako1.Entity import DeltaEntity


class DeltaClipRectangle(DeltaEntity):

    def get_ratio(self):
        return self._ratio

    def get_rectangle(self):
        return self._rectangle

    def reset(self, width, height):
        width_ratio = width/self._monitor_geometry.width
        height_ratio = height/self._monitor_geometry.height
        self._ratio = min(width_ratio, height_ratio)
        self._rectangle = Gdk.Rectangle()
        self._rectangle.width = self._monitor_geometry.width*self._ratio
        self._rectangle.height = self._monitor_geometry.height*self._ratio
        self._rectangle.x = (width - self._rectangle.width)/2
        self._rectangle.y = (height - self._rectangle.height)/2

    def __init__(self, parent):
        self._parent = parent
        for monitor in Gdk.Display.get_default().get_monitors():
            self._monitor_geometry = monitor.get_geometry()
            break
