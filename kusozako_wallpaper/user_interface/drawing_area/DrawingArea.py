# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ApplicationSignals
from .ClipRectangle import DeltaClipRectangle
from .BackgroundPainter import DeltaBackgroundPainter
from .PixbufPainter import DeltaPixbufPainter

SIGNALS = [
    ApplicationSignals.PIXBUF_CHANGED,
    ApplicationSignals.CONFIG_CHANGED
]


class DeltaDrawingArea(Gtk.DrawingArea, DeltaEntity):

    def _delta_info_clip_ratio(self):
        return self._clip_rectangle.get_ratio()

    def _delta_info_clip_rectangle(self):
        return self._clip_rectangle.get_rectangle()

    def _draw_func(self, drawing_area, cairo_context, width, height):
        self._clip_rectangle.reset(width, height)
        self._background_painter.paint(cairo_context, width, height)
        self._pixbuf_painter.paint(cairo_context, width, height)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal not in SIGNALS:
            return
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self, hexpand=True, vexpand=True)
        self._clip_rectangle = DeltaClipRectangle(self)
        self._background_painter = DeltaBackgroundPainter(self)
        self._pixbuf_painter = DeltaPixbufPainter(self)
        self.set_draw_func(self._draw_func)
        self._raise("delta > add to container", self)
        self._raise("delta > register application object", self)
