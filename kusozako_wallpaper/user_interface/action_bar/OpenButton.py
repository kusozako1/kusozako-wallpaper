# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Adw
from gi.repository import Gio
from kusozako1.const import UserSpecialDirectories
from kusozako1.const import MainWindowSignals
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog


class DeltaOpenButton(Gtk.Button, DeltaEntity):

    def _delta_call_dialog_response(self, gfile):
        parent_gfile = gfile.get_parent()
        settings_data = "image", "source_directory", parent_gfile.get_path()
        self._raise("delta > settings", settings_data)
        user_data = MainWindowSignals.FILE_OPENED, gfile
        self._raise("delta > main window signal", user_data)

    def _on_clicked(self, button):
        query = "image", "source_directory", UserSpecialDirectories.PICTURES
        path = self._enquiry("delta > settings", query)
        gfile = Gio.File.new_for_path(path)
        if not gfile.query_exists():
            gfile = Gio.File.new_for_path(UserSpecialDirectories.PICTURES)
        DeltaFileDialog.select_file(
            self,
            title=_("Select Background Image"),
            pixbuf_formats_only=True,
            default_directory=gfile,
            )

    def __init__(self, parent):
        self._parent = parent
        button_content = Adw.ButtonContent.new()
        button_content.set_icon_name("image-x-generic-symbolic")
        button_content.set_label(_("Open Image File"))
        Gtk.Button.__init__(
            self,
            margin_top=4,
            margin_bottom=4,
            margin_start=4,
            margin_end=4,
            )
        self.set_child(button_content)
        self.add_css_class("suggested-action")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
