# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .OpenButton import DeltaOpenButton
from .ConfigButton import DeltaConfigButton
from .ApplyButton import DeltaApplyButton


class DeltaActionBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, 48)
        DeltaOpenButton(self)
        DeltaConfigButton(self)
        self.append(Gtk.Box(hexpand=True))
        DeltaApplyButton(self)
        self.add_css_class("kusozako-primary-surface")
        self._raise("delta > add to container", self)
