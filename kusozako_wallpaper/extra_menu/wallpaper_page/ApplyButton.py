# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ApplicationSignals


class DeltaApplyButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = ApplicationSignals.APPLY, None
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Apply"),
            margin_start=8,
            margin_end=8,
            margin_top=8,
            margin_bottom=8,
            opacity=0.8,
            )
        self.add_css_class("destructive-action")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", Gtk.Box(vexpand=True))
        self._raise("delta > add to container", self)
