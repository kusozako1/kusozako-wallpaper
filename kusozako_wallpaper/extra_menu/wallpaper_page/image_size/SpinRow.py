# (c) copyright 2024-2025, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ApplicationSignals
from kusozako_wallpaper.const import ConfigKeys
from kusozako_wallpaper.const import ImageSize


class DeltaSpinRow(DeltaEntity):

    __config_key__ = ConfigKeys.IMAGE_SIZE

    def _refresh(self, align):
        self._spin_row.set_value(align)

    def _raise_signal(self, param):
        user_data = ApplicationSignals.CHANGE_CONFIG, param
        self._raise("delta > application signal", user_data)

    def _on_changed(self, spin_row):
        param = self.__config_key__, spin_row.get_value()
        self._raise_signal(param)
        additional_param = ConfigKeys.IMAGE_SIZE_TYPE, ImageSize.USER_DEFINED
        self._raise_signal(additional_param)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != ApplicationSignals.CONFIG_CHANGED:
            return
        key, align = param
        if key != self.__config_key__:
            return
        self._refresh(align)

    def __init__(self, parent):
        self._parent = parent
        self._spin_row = Adw.SpinRow(
            numeric=True,
            digits=2,
            )
        self._spin_row.set_range(0, 10)
        adjustment = self._spin_row.get_adjustment()
        adjustment.props.step_increment = 0.01
        align = self._enquiry("delta > config", self.__config_key__)
        self._refresh(align)
        self._spin_row.connect("changed", self._on_changed)
        self._raise("delta > register application object", self)
        self._raise("delta > add to container", self._spin_row)
