# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .XAlignButton import AlfaXAlignButton


class DeltaCenterButton(AlfaXAlignButton):

    LABEL = _("Center")
    MATCH_VALUE = 0.5
