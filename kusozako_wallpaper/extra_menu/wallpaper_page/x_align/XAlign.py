# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.Label import DeltaLabel
from kusozako1.overlay_item.Separator import DeltaSeparator
from .LeftButton import DeltaLeftButton
from .CenterButton import DeltaCenterButton
from .RightButton import DeltaRightButton
from .SpinRow import DeltaSpinRow


class EchoXAlign:

    def __init__(self, parent):
        DeltaSeparator(parent)
        DeltaLabel.new_for_label(parent, "X Align")
        DeltaLeftButton(parent)
        DeltaCenterButton(parent)
        DeltaRightButton(parent)
        DeltaSpinRow(parent)
