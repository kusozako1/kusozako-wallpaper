# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .XAlignButton import AlfaXAlignButton


class DeltaLeftButton(AlfaXAlignButton):

    LABEL = _("Left")
    MATCH_VALUE = 0
