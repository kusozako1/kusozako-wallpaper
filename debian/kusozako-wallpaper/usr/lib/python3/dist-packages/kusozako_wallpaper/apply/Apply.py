# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ApplicationSignals
from .Surface import DeltaSurface
from .Pixbuf import DeltaPixbuf

from gi.repository import Gio


class DeltaApply(DeltaEntity):

    def _apply(self):
        surface = DeltaSurface(self)
        pixbuf = DeltaPixbuf(self)
        cairo_context = surface.get_cairo_context()
        pixbuf.paint(cairo_context)
        path = surface.save()
        if path is None:
            return
        command = ["swaymsg", "output", "'*'", "background", path, "fill"]
        subprocess = Gio.Subprocess.new(command, Gio.SubprocessFlags.NONE)
        subprocess.wait(None)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != ApplicationSignals.APPLY:
            return
        self._apply()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application object", self)
