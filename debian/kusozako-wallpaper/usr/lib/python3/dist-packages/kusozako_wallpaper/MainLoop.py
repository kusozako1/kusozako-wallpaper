# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Transmitter import FoxtrotTransmitter
from kusozako1.main_loop.MainLoop import AlfaMainLoop
from . import APPLICATION_DATA
from .config.Config import DeltaConfig
from .current_image.CurrentImage import DeltaCurrentImage
from .apply.Apply import DeltaApply
from .user_interface.UserInterface import DeltaUserInterface
from .extra_menu.ExtraMenu import EchoExtraMenu


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_register_application_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_application_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_info_current_image(self):
        return self._current_image.get_current_image()

    def _delta_info_config(self, key):
        return self._config[key]

    def _delta_call_loopback_application_window_ready(self, parent):
        self._transmitter = FoxtrotTransmitter()
        self._current_image = DeltaCurrentImage(parent)
        self._config = DeltaConfig(parent)
        DeltaApply(parent)
        DeltaUserInterface(parent)
        EchoExtraMenu(parent)

    def _delta_info_data(self, key):
        return APPLICATION_DATA.get(key, None)
