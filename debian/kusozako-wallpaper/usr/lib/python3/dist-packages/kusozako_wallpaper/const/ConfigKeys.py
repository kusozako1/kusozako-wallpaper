# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

IMAGE_SIZE_TYPE = "image-size-type"
IMAGE_SIZE = "image-size"
IMAGE_ALIGN_X = "image-align-x"
IMAGE_ALIGN_Y = "image-align-y"
BACKGROUND_COLOR = "background-color"
