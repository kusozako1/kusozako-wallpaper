# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

CHANGE_CONFIG = "change-config"             # key, value
PIXBUF_CHANGED = "pixbuf-changed"           # None
CONFIG_CHANGED = "config-changed"           # (key, new-value)
APPLY = "apply"                             # None
