# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .drawing_area.DrawingArea import DeltaDrawingArea
from .action_bar.ActionBar import DeltaActionBar


class DeltaUserInterface(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaDrawingArea(self)
        DeltaActionBar(self)
        self._raise("delta > add to container", self)
