# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Adw
from kusozako1.const import MainWindowSignals
from kusozako1.const import OverlayPage
from kusozako1.Entity import DeltaEntity


class DeltaConfigButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = MainWindowSignals.SHOW_EXTRA_PRIMARY_MENU, "wallpaper"
        self._raise("delta > main window signal", user_data)
        user_data = MainWindowSignals.SHOW_OVERLAY, OverlayPage.PRIMARY_MENU
        self._raise("delta > main window signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        button_content = Adw.ButtonContent.new()
        button_content.set_icon_name("preferences-other-symbolic")
        button_content.set_label(_("Config"))
        Gtk.Button.__init__(
            self,
            margin_top=4,
            margin_bottom=4,
            margin_start=4,
            margin_end=4,
            )
        self.set_child(button_content)
        self.add_css_class("suggested-action")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
