# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Adw
from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ApplicationSignals


class DeltaApplyButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = ApplicationSignals.APPLY, None
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        button_content = Adw.ButtonContent.new()
        button_content.set_icon_name("emblem-ok-symbolic")
        button_content.set_label(_("Apply"))
        Gtk.Button.__init__(
            self,
            margin_top=4,
            margin_bottom=4,
            margin_start=4,
            margin_end=4,
            )
        self.set_child(button_content)
        self.add_css_class("destructive-action")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
