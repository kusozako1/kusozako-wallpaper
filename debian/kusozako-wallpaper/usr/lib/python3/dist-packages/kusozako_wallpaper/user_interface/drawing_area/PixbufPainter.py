# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import GdkPixbuf
from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ConfigKeys


class DeltaPixbufPainter(DeltaEntity):

    def _clip(self, cairo_context):
        clip_rectangle = self._enquiry("delta > clip rectangle")
        cairo_context.rectangle(
            clip_rectangle.x,
            clip_rectangle.y,
            clip_rectangle.width,
            clip_rectangle.height,
            )
        cairo_context.clip()

    def _center(self, cairo_context, width, height, pixbuf):
        clip_ratio = self._enquiry("delta > clip ratio")
        image_size = self._enquiry("delta > config", ConfigKeys.IMAGE_SIZE)
        scaled_pixbuf = pixbuf.scale_simple(
            pixbuf.get_width()*clip_ratio*image_size,
            pixbuf.get_height()*clip_ratio*image_size,
            GdkPixbuf.InterpType.BILINEAR,
            )
        x_align = self._enquiry("delta > config", ConfigKeys.IMAGE_ALIGN_X)
        y_align = self._enquiry("delta > config", ConfigKeys.IMAGE_ALIGN_Y)
        clip_rectangle = self._enquiry("delta > clip rectangle")
        x_shift = (width - clip_rectangle.width)/2
        y_shift = (height - clip_rectangle.height)/2
        x = (clip_rectangle.width-scaled_pixbuf.get_width())*x_align+x_shift
        y = (clip_rectangle.height-scaled_pixbuf.get_height())*y_align+y_shift
        Gdk.cairo_set_source_pixbuf(cairo_context, scaled_pixbuf, x, y)
        cairo_context.paint()

    def paint(self, cairo_context, width, height):
        pixbuf = self._enquiry("delta > current image")
        if pixbuf is None:
            return
        self._clip(cairo_context)
        self._center(cairo_context, width, height, pixbuf)

    def __init__(self, parent):
        self._parent = parent
