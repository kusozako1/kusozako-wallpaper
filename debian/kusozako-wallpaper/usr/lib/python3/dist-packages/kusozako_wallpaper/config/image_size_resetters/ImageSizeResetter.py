# (c) copyright 2025, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ConfigKeys
from kusozako_wallpaper.const import ApplicationSignals


class AlfaImageSizeResetter(DeltaEntity):

    __config_value__ = "define config value here."

    def _get_new_ratio(self, pixbuf):
        raise NotImplementedError()

    def _on_signal_received(self):
        pixbuf = self._enquiry("delta > current image")
        if pixbuf is None:
            return
        new_ratio = self._get_new_ratio(pixbuf)
        self._raise("delta > image size changed", new_ratio)

    def _check_config_param(self, param):
        key, value = param
        if key != ConfigKeys.IMAGE_SIZE_TYPE:
            return False
        if value != self.__config_value__:
            return False
        return True

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != ApplicationSignals.CHANGE_CONFIG:
            return
        if not self._check_config_param(param):
            return
        self._on_signal_received()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register application object", self)
