# (c) copyright 2025, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_wallpaper.const import ImageSizeTypes
from .ImageSizeResetter import AlfaImageSizeResetter


class DeltaOriginal(AlfaImageSizeResetter):

    __config_value__ = ImageSizeTypes.ORIGINAL

    def _get_new_ratio(self, pixbuf):
        return 1
