# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_wallpaper.const import ConfigKeys
from kusozako_wallpaper.const import ImageSizeTypes
from kusozako_wallpaper.const import ApplicationSignals
from .image_size_resetters.ImageSizeResetters import DeltaImageSizeResetters


class DeltaConfig(DeltaEntity):

    def _delta_call_image_size_changed(self, ratio):
        self._set_item(ConfigKeys.IMAGE_SIZE, ratio)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != ApplicationSignals.CHANGE_CONFIG:
            return
        key, value = param
        self._set_item(key, value)

    def _set_item(self, key, value):
        self._config[key] = value
        user_data = ApplicationSignals.CONFIG_CHANGED, (key, value)
        self._raise("delta > application signal", user_data)

    def __getitem__(self, key):
        return self._config[key]

    def __init__(self, parent):
        self._parent = parent
        self._image_size_resetters = DeltaImageSizeResetters(self)
        self._config = {
            ConfigKeys.IMAGE_SIZE_TYPE: ImageSizeTypes.ORIGINAL,
            ConfigKeys.IMAGE_SIZE: 1,
            ConfigKeys.IMAGE_ALIGN_X: 0.5,
            ConfigKeys.IMAGE_ALIGN_Y: 0.5,
            ConfigKeys.BACKGROUND_COLOR: (1, 1, 1, 1),
            }
        self._raise("delta > register application object", self)
