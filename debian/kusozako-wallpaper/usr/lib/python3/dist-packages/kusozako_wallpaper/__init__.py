# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale

VERSION = "2025.02.19"
APPLICATION_NAME = "kusozako-wallpaper"
APPLICATION_ID = "com.gitlab.kusozako1.Wallpaper"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
)

LONG_DESCRIPTION = """wallpaper setter for kusozako project"""

APPLICATION_DATA = {
    "version": VERSION,
    "min-files": 0,
    "max-files": 1,
    "mime": ["image/jpg", "image/jpeg", "image/png", "image/webp"],
    "application-name": APPLICATION_NAME,
    "rdnn-name": APPLICATION_ID,
    "application-id": APPLICATION_ID,
    "long-description": LONG_DESCRIPTION
}
