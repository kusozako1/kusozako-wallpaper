# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_wallpaper.const import ApplicationSignals


class DeltaFileOpened(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, gfile = user_data
        if signal != MainWindowSignals.FILE_OPENED:
            return
        self._raise("delta > setup current image", gfile)
        user_data = ApplicationSignals.PIXBUF_CHANGED, None
        self._raise("delta > application signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register main window signal object", self)
