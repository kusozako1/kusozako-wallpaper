# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity


class DeltaInitialSetup(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        files = self._enquiry("delta > command line files")
        if len(files) != 1:
            return
        gfile = files[0]
        self._raise("delta > setup current image", gfile)
