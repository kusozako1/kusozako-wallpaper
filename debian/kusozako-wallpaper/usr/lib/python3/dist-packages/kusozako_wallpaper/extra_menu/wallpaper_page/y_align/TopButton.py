# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .YAlignButton import AlfaYAlignButton


class DeltaTopButton(AlfaYAlignButton):

    LABEL = _("Top")
    MATCH_VALUE = 0
