# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .XAlignButton import AlfaXAlignButton


class DeltaRightButton(AlfaXAlignButton):

    LABEL = _("Right")
    MATCH_VALUE = 1
