# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_wallpaper.alfa.SpinRow import AlfaSpinRow
from kusozako_wallpaper.const import ConfigKeys


class DeltaSpinRow(AlfaSpinRow):

    __config_key__ = ConfigKeys.IMAGE_ALIGN_Y
