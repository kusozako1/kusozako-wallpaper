# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.Label import DeltaLabel
from .OriginalButton import DeltaOriginalButton
from .FitToWidthButton import DeltaFitToWidthButton
from .FitToHeightButton import DeltaFitToHeightButton
from .SpinRow import DeltaSpinRow


class EchoImageSize:

    def __init__(self, parent):
        DeltaLabel.new_for_label(parent, _("Image Size"))
        DeltaOriginalButton(parent)
        DeltaFitToWidthButton(parent)
        DeltaFitToHeightButton(parent)
        DeltaSpinRow(parent)
