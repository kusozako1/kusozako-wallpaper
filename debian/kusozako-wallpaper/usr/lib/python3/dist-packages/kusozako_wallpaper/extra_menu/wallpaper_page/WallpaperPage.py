# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.sub_page.SubPage import AlfaSubPage
from kusozako1.widget.overlay_item.Spacer import DeltaSpacer
from .image_size.ImageSize import EchoImageSize
from .x_align.XAlign import EchoXAlign
from .y_align.YAlign import EchoYAlign
from .ApplyButton import DeltaApplyButton


class DeltaWallpaperPage(AlfaSubPage):

    PAGE_NAME = "wallpaper"

    def _on_initialize(self):
        EchoImageSize(self)
        EchoXAlign(self)
        EchoYAlign(self)
        DeltaSpacer(self)
        DeltaApplyButton(self)
