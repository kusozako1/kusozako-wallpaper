# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.overlay_item.Label import DeltaLabel
from kusozako1.overlay_item.Separator import DeltaSeparator
from .TopButton import DeltaTopButton
from .CenterButton import DeltaCenterButton
from .BottomButton import DeltaBottomButton
from .SpinRow import DeltaSpinRow


class EchoYAlign:

    def __init__(self, parent):
        DeltaSeparator(parent)
        DeltaLabel.new_for_label(parent, _("Y Align"))
        DeltaTopButton(parent)
        DeltaCenterButton(parent)
        DeltaBottomButton(parent)
        DeltaSpinRow(parent)
