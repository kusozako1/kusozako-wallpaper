# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_wallpaper.const import ConfigKeys
from kusozako_wallpaper.alfa.ConfigButton import AlfaConfigButton


class AlfaXAlignButton(AlfaConfigButton):

    LABEL = "define label here."
    CONFIG_KEY = ConfigKeys.IMAGE_ALIGN_X
    MATCH_VALUE = "define match type here."
