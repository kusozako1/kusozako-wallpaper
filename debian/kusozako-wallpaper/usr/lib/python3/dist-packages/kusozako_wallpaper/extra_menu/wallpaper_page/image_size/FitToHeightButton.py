# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_wallpaper.const import ImageSizeTypes
from .ImageSizeTypeButton import AlfaImageSizeTypeButton


class DeltaFitToHeightButton(AlfaImageSizeTypeButton):

    LABEL = _("Fit to Height")
    MATCH_VALUE = ImageSizeTypes.FIT_TO_HEIGHT
