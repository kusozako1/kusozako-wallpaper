# (c) copyright 2023, takeda.nemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .WallpaperButton import DeltaWallpaperButton
from .wallpaper_page.WallpaperPage import DeltaWallpaperPage


class EchoExtraMenu:

    def __init__(self, parent):
        DeltaWallpaperButton(parent)
        DeltaWallpaperPage(parent)
