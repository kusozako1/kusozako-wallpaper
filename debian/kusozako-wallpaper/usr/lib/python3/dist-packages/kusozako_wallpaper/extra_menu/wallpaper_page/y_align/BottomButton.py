# (c) copyright 2022-2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .YAlignButton import AlfaYAlignButton


class DeltaBottomButton(AlfaYAlignButton):

    LABEL = _("Bottom")
    MATCH_VALUE = 1
