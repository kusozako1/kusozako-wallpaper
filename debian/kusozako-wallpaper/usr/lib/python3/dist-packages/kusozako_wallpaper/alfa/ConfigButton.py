# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1 import SymbolicIcon
from kusozako1.overlay_item.button.Button import AlfaButton
from kusozako_wallpaper.const import ApplicationSignals


class AlfaConfigButton(AlfaButton):

    LABEL = "define label here."
    CONFIG_KEY = "define config key here."
    MATCH_VALUE = "define match type here."

    def _on_clicked(self, button):
        param = self.CONFIG_KEY, self.MATCH_VALUE
        user_data = ApplicationSignals.CHANGE_CONFIG, param
        self._raise("delta > application signal", user_data)

    def _refresh(self, type_):
        if type_ == self.MATCH_VALUE:
            name = "radio-checked-symbolic"
        else:
            name = "radio-symbolic"
        paintable = SymbolicIcon.get_paintable_for_name(name)
        self._start_image.set_from_paintable(paintable)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != ApplicationSignals.CONFIG_CHANGED:
            return
        key, value = param
        if key != self.CONFIG_KEY:
            return
        self._refresh(value)

    def _on_initialize(self):
        type_ = self._enquiry("delta > config", self.CONFIG_KEY)
        self._refresh(type_)
        self._raise("delta > register application object", self)
